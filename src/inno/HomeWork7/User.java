package inno.HomeWork7;

public class User {
    private String firstName;
    private String lastName;
    private int age;
    private boolean isWorker;

    public static class Builder{
        private String firstName;
        private String lastName;
        private int age;
        private boolean isWorker;

        public Builder firstName(String val){
            firstName = val;
            return this;
        }
        public Builder lastName(String val){
            lastName = val;
            return this;
        }
        public Builder age(int val){
            age = val;
            return this;
        }
        public Builder isWorker(boolean val){
            isWorker = val;
            return this;
        }
        public User builder(){
            return new User(this);
        }
    }
    private User(Builder builder){
        firstName = builder.firstName;
        lastName = builder.lastName;
        age = builder.age;
        isWorker = builder.isWorker;
    }
}
