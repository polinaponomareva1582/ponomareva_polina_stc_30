package inno.HomeWork12;

import javax.swing.*;

public class HashSet<V> implements Set<V> {
    private static final int DEFAULT_SIZE = 16;

    private SetEntry<V> entries[] = new SetEntry[DEFAULT_SIZE];

    private static class SetEntry<V> {
        V value;
        SetEntry<V> next;

        public SetEntry(V value) {
            this.value = value;
        }
    }

    @Override
    public void add(V value) {
        SetEntry<V> newSetEntry = new SetEntry<>(value);
        int index = value.hashCode() & (entries.length - 1);
        if (entries[index] == null) {
            entries[index] = newSetEntry;
        } else {
            SetEntry<V> current = entries[index];
            boolean check = this.contains(value);
            if (!check) {
                current.next = newSetEntry;
            } else {
                System.out.println("Элемент " + value + " существует!");
            }
        }
    }

    @Override
    public boolean contains(V value) {
        int index = value.hashCode() & (entries.length - 1);
        SetEntry<V> current = entries[index];
        while (current != null) {
            if (current.value.equals(value)) {
                return true;
            }
            current = current.next;
        }
        return false;
    }
}
