package inno.HomeWork8;

public class Ellipse extends Figure {

    public Ellipse(int a, int b) {
        super(a,b);
    }

    @Override
    public void P(){
        super.P();
        System.out.println("эллипса: "+(4*Math.PI*getA()*getB()+Math.pow((getA()-getB()),2))/(getA()+getB()));
    }

    @Override
    public void S(){
        super.S();
        System.out.println("эллипса: "+Math.PI*getA()*getB());
    }
}
