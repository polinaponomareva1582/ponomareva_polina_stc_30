package inno.HomeWork13.teacher;

import inno.HomeWork13.EducationDAO;
import inno.HomeWork13.Mapper;
import inno.HomeWork13.teacher.Teacher;
import inno.HomeWork13.utils.IdGenerator;
import java.io.*;
import java.text.ParseException;

public class TeacherDAO implements EducationDAO<Teacher> {

    private String fileName;
    private IdGenerator idGenerator;
    private Teacher teacher;

    public TeacherDAO(String fileName, IdGenerator idGenerator) {
        this.fileName = fileName;
        this.idGenerator = idGenerator;
    }

    private Mapper<Teacher, String> teacherToStringMapper = teacher ->
            teacher.getId() + ", " +
                    teacher.getFirstName() + ", " +
                    teacher.getLastName() + ", " +
                    teacher.getWorkTime() + "\r\n";

    private Mapper<String, Teacher> stringToTeacherMapper = string -> {
        String data[] = string.split(", ");
        return new Teacher(data[1], data[2], Integer.parseInt(data[3]));
    };


    @Override
    public void save(Teacher teacher) {
        teacher.setId(idGenerator.nextId());
        try {
            OutputStream outputStream = new FileOutputStream(fileName, true);
            outputStream.write(teacherToStringMapper.map(teacher).getBytes());
            outputStream.close();
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException(e);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Teacher find(Long entity) {
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));
            String current = bufferedReader.readLine();
            while (current != null) {
                if(current.split(", ")[0].equals(entity.toString())){
                    return stringToTeacherMapper.map(current);

                } else {
                    current = bufferedReader.readLine();
                }
            }
            bufferedReader.close();
            return null;
        } catch (IOException | ParseException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
