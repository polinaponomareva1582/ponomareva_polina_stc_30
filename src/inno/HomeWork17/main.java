package inno.HomeWork17;

import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

public class main {
    public static void main(String[] args) throws InterruptedException {
        Integer[] numbers = new Integer[100];

        Random random = new Random();
        for (int i = 0; i < 100; i++) {
            numbers[i] = (random.nextInt(100));
        }
        int checkSum = 0;
        for (int i = 0; i < 100; i++) {
            checkSum += numbers[i];
        }
        System.out.println(checkSum);

        AtomicInteger sum = new AtomicInteger(0);
        Runnable taskPart1 = () -> {
            for (int i = 0; i < 25; i++) {
                sum.set(numbers[i]);
            }
        };

        Runnable taskPart2 = () -> {
            for (int i = 25; i < 50; i++) {
                sum.addAndGet(numbers[i]);
            }
        };

        Runnable taskPart3 = () -> {
            for (int i = 50; i < 75; i++) {
                sum.addAndGet(numbers[i]);
            }
        };
        Runnable taskPart4 = () -> {
            for (int i = 75; i < 100; i++) {
                sum.addAndGet(numbers[i]);
            }
        };
        Thread thread1 = new Thread(taskPart1);
        thread1.start();
        Thread thread2 = new Thread(taskPart2);
        thread2.start();
        Thread thread3 = new Thread(taskPart3);
        thread3.start();
        Thread thread4 = new Thread(taskPart4);
        thread4.start();

        Thread.sleep(5000);
        System.out.println(sum.get());
    }
}
