package inno.HomeWork6;

import java.util.Random;

public class Channel {
    private int channelNumber;
    private Program programs[];
    private int programsCount;

    public Channel(int channelNumber){
        this.channelNumber = channelNumber;
        this.programs = new Program[10];
    }

    public int getChannelNumber() {
        return channelNumber;
    }

    public void addProgramToChannel(Program program){
        programs[programsCount] = program;
        programsCount++;
    }

    public void showRandomProgram(){
        Random random = new Random();
        //int k = (int)Math.random() * programs.length;
        int k = random.nextInt(programsCount);
        programs[k].startProgram(this);
    }
}
