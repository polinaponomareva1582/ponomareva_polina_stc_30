package inno.HomeWork11;

public class ArrayList<E> implements List<E> {
    // контстанта для начального размера массива
    private static final int DEFAULT_SIZE = 10;
    // поле, представляющее собой массив, в котором мы храним элементы
    private E data[];
    // количество элементов в массиве (count != length)
    private int count;

    public ArrayList() {
        this.data = (E[]) new Object[DEFAULT_SIZE];
    }

    private class ArrayListIterator implements Iterator<E> {

        private int current = 0;

        @Override
        public E next() {
            E value = data[current];
            current++;
            return value;
        }

        @Override
        public boolean hasNext() {
            return current < count;
        }
    }

    @Override
    public E get(int index) {
        if (index < count) {
            return this.data[index];
        }
        System.err.println("Вышли за пределы массива");// throw new IllegalArgumentException();
        return null;
    }

    @Override
    public int indexOf(E element) {
        for (int i = 0; i < count; i++) {
            if (data[i].equals(element)) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public void removeByIndex(int index) {
        for (int i = index; i < count - 1; i++) {
            this.data[i] = this.data[i + 1];
        }
        this.count--;
        this.data[count] = null;
    }

    @Override
    public void insert(E element, int index) {
        for (int i = count; i > index; i--) {
            if (count == data.length - 1) {
                resize();
            }
            this.data[i] = this.data[i - 1];
        }
        count++;
        this.data[index] = element;
    }

    @Override
    public boolean contains(E element) {
        return indexOf(element) != -1;
    }

    @Override
    public void add(E element) {
        if (count == data.length - 1) {
            resize();
        }
        data[count] = element;
        count++;
    }

    private void resize() {
        int oldLength = this.data.length;
        //  127(10) -> 1111111(2)
        //  1111111 >> 1 -> 0111111 -> 63
        // N >> K -> N / 2^K
        int newLength = oldLength + (oldLength >> 1); // oldLength + oldLength / 2;
        E newData[] = (E[]) new Object[newLength];

        System.arraycopy(this.data, 0, newData, 0, oldLength);

        this.data = newData;
    }


    @Override
    public int size() {
        return this.count;
    }

    @Override
    public void removeFirst(E element) {
        int indexOfRemovingElement = indexOf(element);

        for (int i = indexOfRemovingElement; i < count - 1; i++) {
            this.data[i] = this.data[i + 1];
        }

        this.count--;
    }

    // реализация метода получения объекта
    @Override
    public Iterator iterator() {
        // CONCRETE PRODUCT
        return new ArrayListIterator();
    }

}
