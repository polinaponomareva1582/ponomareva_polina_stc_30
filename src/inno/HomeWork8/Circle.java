package inno.HomeWork8;

public class Circle extends Figure{

    public Circle(int a) {
        super(a);
    }

    @Override
    public void P(){
        super.P();
        System.out.println("круга: "+2*Math.PI*getA());
    }

    @Override
    public void S(){
        super.S();
        System.out.println("круга: "+Math.PI*getA()*getA());
    }
}
