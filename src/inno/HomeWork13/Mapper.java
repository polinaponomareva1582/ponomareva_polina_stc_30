package inno.HomeWork13;

import java.text.ParseException;

/**
 * 19.11.2020
 * 28. DAO
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface Mapper<X, Y> {
    Y map(X x) throws ParseException;
}

