package inno.Homework10;

public class LinkedList implements List {

    private class LinkedListIterator implements Iterator {

        private int current = 0;
        private Node first;

        @Override
        public int next() {
            int value = get(current);
            current++;
            return value;
        }

        @Override
        public boolean hasNext() {
            return current < count;
        }
    }

    private Node first;
    private Node last;

    private int count;

    private static class Node {
        int value;
        Node next;

        public Node(int value) {
            this.value = value;
        }
    }

    @Override
    public int get(int index) {
        if (index >= 0 && index < count && first != null) {
            int i = 0;
            Node current = this.first;

            while (i < index) {
                current = current.next;
                i++;
            }

            return current.value;
        }
        System.err.println("Такого элемента нет");
        return -1;
    }

    @Override
    public int indexOf(int element) {
        int i = 0;
        Node current = this.first;

        while (current != null && current.value != element) {
            current = current.next;
            i++;
        }

        if (current == null) {
            return -1;
        } else {
            return i;
        }
    }

    @Override
    public void removeByIndex(int index) {
        int i = 0;
        Node previousNode = this.first;
        Node nextNode = this.first;

        if(index==0){
            this.removeFirst(index);
        } else {

        while (i < index-1) {
            previousNode = previousNode.next;
            i++;
        }
        i=0;

        while (i < index+1) {
            nextNode = nextNode.next;
            i++;
        }
        previousNode.next = nextNode;
        count--;
        }
    }

    @Override
    public void insert(int element, int index) {
        int i = 0;
        Node previousNode = this.first;
        Node nextNode = this.first;
        Node newNode = new Node(element);

        if(index==0){
            this.first=newNode;
            this.first.next=nextNode;
        } else {

            while (i < index - 1) {
                previousNode = previousNode.next;
                i++;
            }

            i = 0;
            while (i < index) {
                nextNode = nextNode.next;
                i++;
            }
            previousNode.next = newNode;
            newNode.next = nextNode;
        }
        count++;
    }

    // Добавление в конец - О(1)
    @Override
    public void add(int element) {
        Node newNode = new Node(element);
        if (first == null) {
            first = newNode;
            last = newNode;
        } else {
            // следующий после последнего - новый узел
            last.next = newNode;
            // новый узел теперь последний
            last = newNode;
        }
        count++;
    }

    public LinkedList reverse(){
        LinkedList reverseList = new LinkedList();
        for (int i = count - 1; i >= 0; i--) {

            // Append the elements in reverse order
            reverseList.add(this.get(i));
        }
        // Return the reversed arraylist
        return reverseList;
    }

    @Override
    public boolean contains(int element) {
        return indexOf(element)!= -1;
    }

    @Override
    public int size() {
        return count;
    }

    @Override
    public void removeFirst(int element) {
        this.first=this.first.next;
        count--;
    }

    // реализация метода получения объекта
    @Override
    public Iterator iterator() {
        return new LinkedListIterator();
    }
}
