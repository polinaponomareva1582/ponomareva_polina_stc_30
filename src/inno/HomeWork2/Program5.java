package inno.HomeWork2;

import java.util.Arrays;

public class Program5 {
    public static void main(String[] args) {
        int[] array = {5,1,6,9,3,0,10,2};
        for(int i=0;i<array.length-1;i++){
            for(int j=array.length-1;j>i;j--){
                if(array[j]<array[j-1]){
                    int temp = array[j];
                    array[j]=array[j-1];
                    array[j-1]=temp;
                }
            }
        }
        System.out.println(Arrays.toString(array));
    }
}
