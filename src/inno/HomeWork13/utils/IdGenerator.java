package inno.HomeWork13.utils;

public interface IdGenerator {
    Long nextId();
}
