package inno.HomeWork14;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Stream;

public class main {
    public static void main(String[] args) throws IOException {
        List<Car> cars = new ArrayList<>();

        BufferedReader reader = new BufferedReader(new FileReader("src/inno/HomeWork14/cars.txt"));
        String line = reader.readLine();

        while (line != null) {
            //System.out.println(line);
            String data[] = line.split(" ");
            Car car = new Car(data[0],data[1],data[2],Integer.parseInt(data[3]),Integer.parseInt(data[4]));
            cars.add(car);
            line = reader.readLine();
        }

        Stream<Car> stream = cars.stream();
        //stream.filter(car->car.getColor().equals("black") || car.getMileage().equals(0)).forEach(car->System.out.println(car.getNumber()));
        //System.out.println(stream.filter(car-> 700000 <= car.getCost() && 800000 >= car.getCost()).map(car -> car.getModel()).distinct().count());
        //stream.sorted(Comparator.comparing(Car::getCost)).limit(1).forEach(car->System.out.println(car.getColor()));
        //System.out.println(stream.filter(car -> car.getModel().equals("Camry")).mapToInt(car->car.getCost()).average().getAsDouble());
    }
}
