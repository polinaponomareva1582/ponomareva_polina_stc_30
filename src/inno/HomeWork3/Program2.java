package inno.HomeWork3;

public class Program2 {
    public static void main(String[] args) {
        int ns[] = {10, 100, 1000, 10000};
        printIntegralResultsForN(0, 10, ns);
    }

    public static double f(double x){
        return x;
    }

    public static double SimpsonMethod(double a, double b, int n){
        double h = (a+b)/n;
        double result = 0;
        for(double x=a+h; x<=b; x=x+2*h){
            result += f(x-h)+4*f(x)+f(x+h);
        }
        return (h/3)*result;
    }

    public static void printIntegralResultsForN(double a, double b, int ns[]) {
        for (int i = 0; i < ns.length; i++) {
            System.out.println("For N = " + ns[i]
                    + ", result = " + SimpsonMethod(a, b, ns[i]));
        }
    }
}
