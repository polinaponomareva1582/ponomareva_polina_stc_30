package inno.HomeWork13;
import inno.HomeWork13.course.Course;
import inno.HomeWork13.course.CourseDAO;
import inno.HomeWork13.lesson.Lesson;
import inno.HomeWork13.lesson.LessonDAO;
import inno.HomeWork13.teacher.Teacher;
import inno.HomeWork13.teacher.TeacherDAO;
import inno.HomeWork13.utils.IdGeneratorFileBasedImpl;

public class main {
    public static void main(String[] args) {

        EducationDAO courseDAO = new CourseDAO("courses.txt",
                new IdGeneratorFileBasedImpl("src/inno/HomeWork13/courses_sequence.txt"));
        EducationDAO teacherDAO = new TeacherDAO("teachers.txt",
                new IdGeneratorFileBasedImpl("src/inno/HomeWork13/teachers_sequence.txt"));
        EducationDAO lessonDAO = new LessonDAO("lessons.txt",
                new IdGeneratorFileBasedImpl("src/inno/HomeWork13/lessons_sequence.txt"));

        Course course1 = new Course( "Course1","1 января 2021","7 января 2021");
        Course course2 = new Course("Course2", "8 января 2021", "15 января 2021");
        courseDAO.save(course1);
        courseDAO.save(course2);

        Lesson lesson1 = new Lesson("Math","1 января 2021 15:00");
        lessonDAO.save(lesson1);

        course1.addLesson(lesson1);

        Teacher teacher1 = new Teacher("Polina","Ponomareva",1);
        teacherDAO.save(teacher1);
        course1.addTeacher(teacher1);
        teacher1.addCourse(course1);

        Course test1 = (Course) courseDAO.find(2L);
        Lesson test2 = (Lesson) lessonDAO.find(4L);
        Teacher test3 = (Teacher) teacherDAO.find(1L);
        Integer i = 0;
    }
}
