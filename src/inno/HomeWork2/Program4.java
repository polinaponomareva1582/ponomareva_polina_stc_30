package inno.HomeWork2;

import java.util.Arrays;
import java.util.Scanner;

public class Program4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] array = new int[n];
        int max = 0;
        int min = n-1;
        for(int i = 0; i < n; i++){
            array[i] = scanner.nextInt();
        }
        for(int i = 0;i < n; i++){
            if(array[i]>array[max]){
                max = i;
            }
            else if(array[min]>array[i]){
                min=i;
            }
        }
        int t = array[min];
        array[min]=array[max];
        array[max]=t;
        System.out.println(Arrays.toString(array));
    }
}
