package inno.HomeWork11;

public interface Iterable<B> {
    Iterator<B> iterator();
}
