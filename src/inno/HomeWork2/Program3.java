package inno.HomeWork2;

import java.util.Scanner;

public class Program3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] array = new int[n];
        int arrayAverage = 0;
        for(int i = 0; i < n; i++){
            array[i] = scanner.nextInt();
            arrayAverage = arrayAverage+array[i];
        }
        arrayAverage=arrayAverage/n;
        System.out.println(arrayAverage);
    }
}
