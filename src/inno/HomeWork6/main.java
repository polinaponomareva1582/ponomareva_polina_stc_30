package inno.HomeWork6;

public class main {
    public static void main(String[] args) {
        // создали пульт
        RemoteController rm = new RemoteController(1);
        // создали телек
        TV tv = new TV("Samsung");
        // объявили каналы
        Channel ch1 = new Channel( 1);
        Channel ch2 = new Channel( 2);
        Channel ch3 = new Channel( 3);
        // объявили программы
        Program p1 = new Program("The late late show");
        Program p2 = new Program("The crosswalk show");
        Program p3 = new Program("Breaking News");
        Program p4 = new Program("Cooking Time");
        Program p5 = new Program("Sezam Street");
        Program p6 = new Program("Pets");
        Program p7 = new Program("Good News");
        Program p8 = new Program("Harry Potter part 1");
        Program p9 = new Program("TopGear");
        Program p10 = new Program("The Grand Tour");

        //подключили пульт к телеку
        rm.connectToTV(tv);
        //настроили каналы
        tv.configChannel(ch1);
        tv.configChannel(ch2);
        tv.configChannel(ch3);
        //определили программы для каналов
        ch1.addProgramToChannel(p1);
        ch1.addProgramToChannel(p2);
        ch3.addProgramToChannel(p3);
        ch2.addProgramToChannel(p4);
        ch1.addProgramToChannel(p5);
        ch2.addProgramToChannel(p6);
        ch3.addProgramToChannel(p7);
        ch3.addProgramToChannel(p8);
        ch1.addProgramToChannel(p9);
        ch2.addProgramToChannel(p10);

        rm.openChannel(tv,1);
        rm.openChannel(tv,2);
        rm.openChannel(tv,3);
    }
}
