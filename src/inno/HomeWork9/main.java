package inno.HomeWork9;

public class main {
    public static void main(String[] args) {
        int[] numbers = {12345, 50780, 183, 10001010};
        String[] lines = {"first1","second12345","abrakadabra"};

        NumbersAndStringsProcessor obj = new NumbersAndStringsProcessor(lines,numbers);

        //разворот числа
        obj.process((NumberProcess) (number)->{
            int result = 0;
            while(number!=0){
                result = result*10 + number%10;
                number=number/10;
            }
            return result;
        });

        //удаление нулей
        obj.process((NumberProcess) (number)->{
            int result = 0;
            int p = 0;
            while(number!=0){
                if(number%10!=0) {
                    result += (number % 10)*Math.pow(10,p);
                    p++;
                }
                number = number / 10;
            }
            return result;
        });

        //замена нечетных на четные
        obj.process((NumberProcess) (number)->{
            int result = 0;
            int p=0;
            while(number!=0){
                if(number%2==1) {
                    result += (number % 10 - 1)*Math.pow(10,p);
                    number = number / 10;
                    p++;
                }
                else {
                    result += (number % 10)*Math.pow(10,p);
                    number = number / 10;
                    p++;
                }
            }
            return result;
        });

        //разворот строки
        obj.process((StringsProcess) (line)->{
            char[] array = line.toCharArray();
            String result = "";
            for (int i = array.length - 1; i >= 0; i--) {
                result = result + array[i];
            }
            return result;
        });

        //убрать все цифры из строки
        obj.process((StringsProcess) (line)->{
           String result="";
           for (int i=0;i<line.length();i++){
               if(!Character.isDigit(line.charAt(i))){
                   result+=line.charAt(i);
               }
           }
           return result;
        });

        //большие буквы

        obj.process((StringsProcess) (line)->{
           return line.toUpperCase();
        });
    }
}
