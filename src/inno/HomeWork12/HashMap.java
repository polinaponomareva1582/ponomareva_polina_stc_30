package inno.HomeWork12;

public class HashMap<K, V> implements Map<K, V> {
    private static final int DEFAULT_SIZE = 16;

    private MapEntry<K, V> entries[] = new MapEntry[DEFAULT_SIZE];

    private static class MapEntry<K, V> {
        K key;
        V value;
        MapEntry<K, V> next;

        public MapEntry(K key, V value) {
            this.key = key;
            this.value = value;
        }
    }

    @Override
    public void put(K key, V value) {
        MapEntry<K, V> newMapEntry = new MapEntry<>(key, value);
        int index = key.hashCode() & (entries.length - 1);
        if (entries[index] == null) {
            entries[index] = newMapEntry;
        } else if (entries[index].key.equals(key)) {
            entries[index].value = value;
        } else {
            MapEntry<K, V> current = entries[index];
            while (current.next != null) {
                current = current.next;
            }
            current.next = newMapEntry;
        }
    }

    @Override
    public V get(K key) {
        int index = key.hashCode() & (entries.length - 1);
        MapEntry<K, V> current = entries[index];
        while (current != null) {
            if (current.key.equals(key)) {
                return current.value;
            }
            current = current.next;
        }
        return null;
    }
}
