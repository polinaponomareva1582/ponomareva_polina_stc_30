package inno.HomeWork6;

public class RemoteController {
    private int id;
    private TV tv;

    public RemoteController(int id){
        this.id = id;
    }

    public void connectToTV(TV tv){
        this.tv = tv;
        this.tv.addController(this);
    }

    public void openChannel(TV tv, int num){
        tv.goToChannel(num);
    }
}
