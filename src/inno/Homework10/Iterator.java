package inno.Homework10;

public interface Iterator {
    // возвращает следующий элемент
    int next();
    // проверяет, есть ли следующий элемент?
    boolean hasNext();
}
