package inno.HomeWork7;

public class main {
    public static void main(String[] args) {
        User user = new User.Builder()
                .firstName("Marsel")
                .lastName("Sidikov")
                .age(26)
                .isWorker(true)
                .builder();
    }
}
