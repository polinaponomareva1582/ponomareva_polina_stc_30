package inno.HomeWork13;

public interface EducationDAO<T> {
    void save(T entity);
    T find(Long key);
}
