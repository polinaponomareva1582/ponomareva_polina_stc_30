package inno.HomeWork11;

public class LinkedList<E> implements List<E> {

    private class LinkedListIterator implements Iterator<E> {

        private int current = 0;
        private Node<E> first;

        @Override
        public E next() {
            E value = get(current);
            current++;
            return value;
        }

        @Override
        public boolean hasNext() {
            return current < count;
        }
    }

    private Node<E> first;
    private Node<E> last;

    private int count;

    private static class Node<F> {
        F value;
        Node<F> next;

        public Node(F value) {
            this.value = value;
        }
    }

    @Override
    public E get(int index) {
        if (index >= 0 && index < count && first != null) {
            int i = 0;
            Node<E> current = this.first;

            while (i < index) {
                current = current.next;
                i++;
            }
            return current.value;
        }
        System.err.println("Такого элемента нет");
        return null;
    }

    @Override
    public int indexOf(E element) {
        int i = 0;
        Node<E> current = this.first;

        while (current != null && !current.value.equals(element)) {
            current = current.next;
            i++;
        }

        if (current == null) {
            return -1;
        } else {
            return i;
        }
    }

    @Override
    public void removeByIndex(int index) {
        int i = 0;
        Node<E> previousNode = this.first;
        Node<E> nextNode = this.first;

        if (index == 0) {
            this.removeFirst(get(index));
        } else {

            while (i < index - 1) {
                previousNode = previousNode.next;
                i++;
            }
            i = 0;

            while (i < index + 1) {
                nextNode = nextNode.next;
                i++;
            }
            previousNode.next = nextNode;
            count--;
        }
    }

    @Override
    public void insert(E element, int index) {
        int i = 0;
        Node<E> previousNode = this.first;
        Node<E> nextNode = this.first;
        Node<E> newNode = new Node(element);

        if (index == 0) {
            this.first = newNode;
            this.first.next = nextNode;
        } else {

            while (i < index - 1) {
                previousNode = previousNode.next;
                i++;
            }

            i = 0;
            while (i < index) {
                nextNode = nextNode.next;
                i++;
            }
            previousNode.next = newNode;
            newNode.next = nextNode;
        }
        count++;
    }

    // Добавление в конец - О(1)
    @Override
    public void add(E element) {
        Node<E> newNode = new Node(element);
        if (first == null) {
            first = newNode;
            last = newNode;
        } else {
            // следующий после последнего - новый узел
            last.next = newNode;
            // новый узел теперь последний
            last = newNode;
        }
        count++;
    }

    public LinkedList<E> reverse() {
        LinkedList<E> reverseList = new LinkedList<>();
        for (int i = count - 1; i >= 0; i--) {

            // Append the elements in reverse order
            reverseList.add(this.get(i));
        }
        // Return the reversed arraylist
        return reverseList;
    }

    @Override
    public boolean contains(E element) {
        return indexOf(element) != -1;
    }

    @Override
    public int size() {
        return count;
    }

    @Override
    public void removeFirst(E element) {
        this.first = this.first.next;
        count--;
    }

    // реализация метода получения объекта
    @Override
    public Iterator iterator() {
        return new LinkedListIterator();
    }
}
