package inno.HomeWork11;

public interface Iterator<A> {
    // возвращает следующий элемент
    A next();
    // проверяет, есть ли следующий элемент?
    boolean hasNext();
}
