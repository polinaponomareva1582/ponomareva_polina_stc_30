package inno.HomeWork13.lesson;

import inno.HomeWork13.course.Course;

public class Lesson {
    private Long id;
    private String LessonName;
    private Course course;
    private String DateTime;

    public Lesson(String lessonName, String dateTime) {
        LessonName = lessonName;
        DateTime = dateTime;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLessonName() {
        return LessonName;
    }

    public void setLessonName(String lessonName) {
        LessonName = lessonName;
    }

    public String getDateTime() {
        return DateTime;
    }

    public void setDateTime(String dateTime) {
        DateTime = dateTime;
    }
}
