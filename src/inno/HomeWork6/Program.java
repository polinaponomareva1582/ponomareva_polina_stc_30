package inno.HomeWork6;

public class Program {
    private String programName;
    private Channel channel;

    public Program(String programName) {
        this.programName = programName;
    }
    public void startProgram(Channel channel){
        this.channel = channel;
        System.out.println("Start '"+ this.programName + "' on " +channel.getChannelNumber()+" channel.");
    }
}
