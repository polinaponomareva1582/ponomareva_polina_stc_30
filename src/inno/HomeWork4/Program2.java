package inno.HomeWork4;

import java.util.Arrays;

public class Program2 {
    public static void main(String[] args) {
        int[] elements = {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15};
        binarySearch(elements, 15);
    }

    public static int binarySearch(int[] array, int element){
        System.out.println("f--> "+Arrays.toString(array)+" mediana --> "+array[array.length/2]);
        if(array[array.length/2]>element){
            return binarySearch(Arrays.copyOfRange(array, 0, array.length/2), element);
        }
        else if(array[array.length/2]<element){
            return binarySearch(Arrays.copyOfRange(array, array.length/2, array.length), element);
        }
        else {
            System.out.println(array[array.length/2]);
            return array[array.length/2];
        }
    }
}
