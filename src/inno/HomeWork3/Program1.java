package inno.HomeWork3;

import java.util.Arrays;
import java.util.Scanner;

public class Program1 {
    public static void main(String[] args) {
        int[] array = {1,2,3,4,5,6};
        int[] array2 = {3,5,1,9,0,2,4,7,6};
        SumOfArrayElements(array);
        ReverseArray();
        int average = Average();
        System.out.println("Average: " + average);
        ChangeMaxMin(array);
        Sort(array2);
        int Num = ArrayToNumber(array2);
        System.out.println("Number from array: "+Num);
    }

    public static void SumOfArrayElements(int a[]){
        int arraySum = 0;
        for(int i = 0; i < a.length; i++){
            arraySum += a[i];
        }
        System.out.println("Sum of all elements in array: "+arraySum);
    }

    public static void ReverseArray(){
        System.out.println("Enter size of array: ");
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] a = new int[n];
        System.out.println("Enter elements of array: ");
        for(int i = 0; i < n; i++){
            a[n-1-i] = scanner.nextInt();
        }
        System.out.println("Array back to front: "+ Arrays.toString(a));
    }

    public static int Average(){
        System.out.println("Enter size of array: ");
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] a = new int[n];
        int arrayAverage = 0;
        System.out.println("Enter elements of array: ");
        for(int i = 0; i < n; i++){
            a[i] = scanner.nextInt();
            arrayAverage = arrayAverage+a[i];
        }
        arrayAverage=arrayAverage/n;
        return arrayAverage;
    }

    public static void ChangeMaxMin(int a[]){
        int max = 0;
        int min = a.length-1;
        for(int i = 0;i < a.length; i++){
            if(a[i]>a[max]){
                max = i;
            }
            else if(a[min]>a[i]){
                min=i;
            }
        }
        int t = a[min];
        a[min]=a[max];
        a[max]=t;
        System.out.println("Array with changed Min and Max: " + Arrays.toString(a));
    }

    public static void Sort(int a[]){
        for(int i=0;i<a.length-1;i++){
            for(int j=a.length-1;j>i;j--){
                if(a[j]<a[j-1]){
                    int temp = a[j];
                    a[j]=a[j-1];
                    a[j-1]=temp;
                }
            }
        }
        System.out.println("Sorted array: " + Arrays.toString(a));
    }

    public static int ArrayToNumber(int a[]){
        int number = 0;
        int n = a.length;
        for(int i=0; i<n; i++){
            number+=a[i]*Math.pow(10,n-1-i);
        }
        return number;
    }
}
