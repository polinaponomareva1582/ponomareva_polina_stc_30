package inno.HomeWork8;

public class Figure implements Scalable, Relocatable{
    private int a;
    private int b;
    private int x = 0;
    private int y = 0;

    public Figure(int a){
        this.a = a;
    }

    public Figure(int a, int b) {
        this.a = a;
        this.b = b;
    }

    public int getA() {
        return a;
    }

    public int getB() {
        return b;
    }

    public void P(){
        System.out.print("Периметр ");
    }

    public void S(){
        System.out.print("Площадь ");
    }

    @Override
    public void changeSize(int k) {
        this.a+=k;
        this.b+=k;
    }

    @Override
    public void changeStartPoint(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public void Points(){
        System.out.println("Точки X и Y: "+this.x+" "+this.y);
    }
}
