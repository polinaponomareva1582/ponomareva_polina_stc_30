package inno.HomeWork2;

import java.util.Scanner;

public class Program1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] array = new int[n];
        int arraySum = 0;
        for(int i = 0; i < n; i++){
            array[i] = scanner.nextInt();
            arraySum += array[i];
        }
        System.out.println(arraySum);
    }
}
