package inno.HomeWork9;

import java.util.Arrays;

public class NumbersAndStringsProcessor {
    private String[] lines;
    private int[] numbers;

    public NumbersAndStringsProcessor(String[] lines, int[] numbers) {
        this.lines = lines;
        this.numbers = numbers;
    }

    public String[] process(StringsProcess function){
        String[] result = new String[lines.length];
        for(int i=0;i<lines.length;i++){
            result[i]=function.process(lines[i]);
        }
        System.out.println(Arrays.toString(result));
        return result;
    }
    public int[] process(NumberProcess function){
        int[] result = new int[numbers.length];
        for (int i=0;i<numbers.length;i++){
            result[i]=function.process(numbers[i]);
        }
        System.out.println(Arrays.toString(result));
        return result;
    }
}
