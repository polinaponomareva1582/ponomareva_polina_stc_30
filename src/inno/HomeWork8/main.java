package inno.HomeWork8;

public class main {
    public static void main(String[] args) {
        Figure one = new Square(5);
        Figure two = new Rectangle(3,5);
        Figure three = new Ellipse(3,4);
        Figure four = new Circle(8);

        Figure figures[] = {one, two, three, four};
        for(int i=0;i<figures.length;i++){
            figures[i].P();
            figures[i].S();
        }

        one.changeSize(2);
        one.P();

        two.changeSize(3);
        two.P();
        two.S();

        one.Points();
        one.changeStartPoint(1,1);
        one.Points();
    }
}
