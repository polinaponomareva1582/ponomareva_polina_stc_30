package inno.HomeWork13.lesson;

import inno.HomeWork13.EducationDAO;
import inno.HomeWork13.Mapper;
import inno.HomeWork13.lesson.Lesson;
import inno.HomeWork13.utils.IdGenerator;

import java.io.*;
import java.text.ParseException;

public class LessonDAO implements EducationDAO<Lesson> {

    private String fileName;
    private IdGenerator idGenerator;

    public LessonDAO(String fileName, IdGenerator idGenerator) {
        this.fileName = fileName;
        this.idGenerator = idGenerator;
    }

    private Mapper<Lesson, String> lessonToStringMapper = lesson ->
            lesson.getId()+ ", " +
            lesson.getLessonName() + ", " +
                    lesson.getDateTime() + "\r\n";

    private Mapper<String, Lesson> stringToLessonMapper = string -> {
        String data[] = string.split(", ");
        return new Lesson(data[1], data[2]);
    };

    @Override
    public void save(Lesson entity) {
        entity.setId(idGenerator.nextId());
        try {
            OutputStream outputStream = new FileOutputStream(fileName, true);
            outputStream.write(lessonToStringMapper.map(entity).getBytes());
            outputStream.close();
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException(e);
        } catch (IOException | ParseException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public Lesson find(Long entity) {
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));
            String current = bufferedReader.readLine();
            while (current != null) {
                if(current.split(", ")[0].equals(entity.toString())){
                    return stringToLessonMapper.map(current);

                } else {
                    current = bufferedReader.readLine();
                }
            }
            bufferedReader.close();
            return null;
        } catch (IOException | ParseException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
