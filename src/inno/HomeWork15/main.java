package inno.HomeWork15;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class main {
    public static int Min(int a, int b) {
        if (a > b)
            return b;
        else
            return a;
    }

    public static int Pivot(String[] a) {
        Random random = new Random();
        int pivot = random.nextInt(a.length);
        return pivot;
    }

    public static String[] QuickSort(String[] a, int p) {
        //System.out.println("Вход с массивом " + Arrays.toString(a));
        List<String> left = new ArrayList<>();
        List<String> newLeft = new ArrayList<>();
        List<String> right = new ArrayList<>();
        List<String> newRight = new ArrayList<>();
        if (a.length >= 2) {
            for (int i = 0; i < a.length; i++) {
                if (i != p) {
                    int min = Min(a[i].length(), a[p].length());
                    int f = 0;
                    while (f != min) {
                        if (a[i].charAt(f) == a[p].charAt(f) && f != min - 1) {
                            f++;
                        } else if (a[i].charAt(f) < a[p].charAt(f)) {
                            left.add(a[i]);
                            f = min;
                        } else {
                            right.add(a[i]);
                            f = min;
                        }
                    }
                }
            }
            left.add(a[p]);
        }
        if (left.size() > 1) {
            newLeft.addAll(Arrays.asList(QuickSort(left.toArray(new String[0]), Pivot(left.toArray(new String[0])))));
        }
        if (right.size() > 1) {
            newRight.addAll(Arrays.asList(QuickSort(right.toArray(new String[0]), Pivot(right.toArray(new String[0])))));
        }
        if (left.size() == 1) {
            newLeft.addAll(left);
        }
        if (right.size() == 1) {
            newRight.addAll(right);
        }
        newLeft.addAll(newRight);
        //System.out.println("Выход с массивом " + Arrays.toString(newLeft.toArray(new String[0])));
        return newLeft.toArray(new String[0]);
    }

    public static void main(String[] args) throws IOException {
        Path path = Paths.get("src/inno/HomeWork15/text.txt");
        String read = Files.readString(path);
        String data[] = read.split(" ");

        String result[] = QuickSort(data, Pivot(data));

        for (int i = 0; i < result.length; i++) {
            System.out.println(result[i]);
        }
    }
}
