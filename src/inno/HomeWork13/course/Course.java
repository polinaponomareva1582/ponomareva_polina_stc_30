package inno.HomeWork13.course;

import inno.HomeWork13.lesson.Lesson;
import inno.HomeWork13.teacher.Teacher;

import java.util.ArrayList;
import java.util.List;

public class Course {
    private Long id;
    private String CourseName;
    private String Start;
    private String Finish;
    private List<Teacher> teachers = new ArrayList<Teacher>();
    private List<Lesson> lessons = new ArrayList<Lesson>();

    public Course(String courseName, String start, String finish) {
        CourseName = courseName;
        Start = start;
        Finish = finish;
    }

    public Long getId() {
        return id;
    }

    public String getCourseName() {
        return CourseName;
    }

    public String getStart() {
        return Start;
    }

    public String getFinish() {
        return Finish;
    }

    public void addLesson(Lesson lesson){
        lessons.add(lesson);
    }
    public void addTeacher(Teacher teacher){
        teachers.add(teacher);
    }
//    public void showLessons(){
//        lessons.forEach(System.out::println);
//    }
    public void setId(Long id) {
        this.id = id;
    }

    public void setCourseName(String courseName) {
        CourseName = courseName;
    }

    public void setStart(String start) {
        Start = start;
    }

    public void setFinish(String finish) {
        Finish = finish;
    }
}
