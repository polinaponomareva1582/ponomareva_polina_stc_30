package inno.HomeWork8;

public class Rectangle extends Figure{

    public Rectangle(int a, int b) {
        super(a, b);
    }

    @Override
    public void P(){
        super.P();
        System.out.println("прямоугольника: "+2*(getA()+getB()));
    }

    @Override
    public void S(){
        super.S();
        System.out.println("прямоугольника: "+getA()*getB());
    }
}
