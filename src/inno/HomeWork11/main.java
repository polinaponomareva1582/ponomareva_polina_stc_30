package inno.HomeWork11;

public class main {
    public static void main(String[] args) {
        List<String> list = new LinkedList<>();

        for (int i = 0; i < 9; i++) {
            list.add(String.valueOf(i));
        }

        System.out.println(list.get(2));
        System.out.println(list.indexOf("2"));
        list.removeByIndex(5);
        list.insert("hello", 4);
        list.insert("world", 5);

//        List<Integer> list = new ArrayList<>();
//
//        list.add(654);
//        list.add(655);
//        list.add(656);
//
//        list.add(657);
//        list.add(658);
//        list.add(659);
//
//        list.add(660);
//        list.add(661);
//        list.add(662);
//
//        System.out.println(list);
//        System.out.println(list.size());
//        System.out.println(list.get(3));
//
//        list.removeByIndex(3);
//
//        System.out.println(list);
//        System.out.println(list.get(3));
//        System.out.println(list.size());
//
//        list.insert(11111111, 5);
//        list.insert(136548232, 6);
//        list.insert(4444444, 2);
//
//        System.out.println(list.indexOf(659));
    }
}
