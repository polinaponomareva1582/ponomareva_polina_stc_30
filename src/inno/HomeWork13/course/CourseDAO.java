package inno.HomeWork13.course;

import inno.HomeWork13.EducationDAO;
import inno.HomeWork13.Mapper;
import inno.HomeWork13.course.Course;
import inno.HomeWork13.utils.IdGenerator;

import java.io.*;
import java.text.ParseException;

public class CourseDAO implements EducationDAO<Course> {

    private String fileName;
    private IdGenerator idGenerator;

    public CourseDAO(String fileName, IdGenerator idGenerator) {
        this.fileName = fileName;
        this.idGenerator = idGenerator;
    }

    private Mapper<Course, String> courseToStringMapper = course ->
            course.getId() + ", " +
                    course.getCourseName() + ", " +
                    course.getStart() + ", " +
                    course.getFinish()+ "\r\n";

    private Mapper<String, Course> stringToCourserMapper = string -> {
        String data[] = string.split(", ");
        return new Course(data[1],data[2],data[3]);
    };

    @Override
    public void save(Course entity) {
        entity.setId(idGenerator.nextId());
        try {
            OutputStream outputStream = new FileOutputStream(fileName, true);
            outputStream.write(courseToStringMapper.map(entity).getBytes());
            outputStream.close();
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException(e);
        } catch (IOException | ParseException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public Course find(Long entity) {
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));
            String current = bufferedReader.readLine();
            while (current != null) {
                if(current.split(", ")[0].equals(entity.toString())){
                    return stringToCourserMapper.map(current);
                } else {
                    current = bufferedReader.readLine();
                }
            }
            bufferedReader.close();
            return null;
        } catch (IOException | ParseException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
