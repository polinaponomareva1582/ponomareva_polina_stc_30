package inno.HomeWork6;

public class TV {
    private String model;
    private Channel channels[];
    private int channelCount;
    private RemoteController controller;

    public TV(String model) {
        this.model = model;
        this.channels = new Channel[5];
    }

    public void addController(RemoteController controller){
        this.controller=controller;
    }

    public void configChannel(Channel channel){
        channels[channelCount] = channel;
        channelCount++;
    }

    public void goToChannel(int num){
        int i=0;
        while(channels[i].getChannelNumber()!=num){
            i++;
        }
        channels[i].showRandomProgram();
    }
}
