package inno.HomeWork13.teacher;
import inno.HomeWork13.course.Course;

import java.util.ArrayList;
import java.util.List;

public class Teacher {
    private Long id;
    private String FirstName;
    private String LastName;
    private Integer WorkTime;
    private List<Course> courses = new ArrayList<Course>();

    public Teacher(String firstName, String lastName, Integer workTime) {
        FirstName = firstName;
        LastName = lastName;
        WorkTime = workTime;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public Integer getWorkTime() {
        return WorkTime;
    }

    public void setWorkTime(Integer workTime) {
        WorkTime = workTime;
    }

    public void addCourse(Course course){
        courses.add(course);
    }
}
