package inno.HomeWork1;


import java.util.Scanner;

public class Program3 {
    public static void main(String[] args) {
        int userNumber = 1;
        int result = 1;
        while(userNumber!=0) {
            Scanner scanner = new Scanner(System.in);
            userNumber = scanner.nextInt();
            int checkNumber = userNumber;
            int digitsSum = 0;
            while(checkNumber!=0){
                digitsSum += checkNumber%10;
                checkNumber=checkNumber/10;
            }
            boolean checkSimple = true;
            for (int i = 2; i < digitsSum/2; i++)
            {
                if (digitsSum % i == 0)
                {
                    checkSimple = false;
                    break;
                }
                checkSimple=true;
            }
            if(checkSimple && userNumber!=0){
                result = result * userNumber;
            }
        }
        System.out.println(result);
    }
}
